#!/bin/bash

# Load settings
# basically, dot-source the conf file.
test -z "${librewolf_dpkg_conf}" && export librewolf_dpkg_conf="$( find "$( dirname "${0}" )" -maxdepth 2 -name "$( basename "${0%%.sh}.conf" )" -print 2>/dev/null | head -n1 )"
test ! -r "${librewolf_dpkg_conf}" && { echo "Unable to load config file, which should be named the same as this script but with a .conf ending. Aborted." 1>&2 ; exit 1 ; }
. "${librewolf_dpkg_conf}"

work_dir=${CI_PROJECT_DIR}/prepared/

# Aborts the script upon any faliure
set -e;

apt update
export DEBIAN_FRONTEND=noninteractive
apt install -y  \
  tzdata \
  cdbs \
  locales \
  debhelper \
  autotools-dev \
  autoconf2.13 \
  zip \
  libx11-dev \
  libx11-xcb-dev \
  libxt-dev \
  libxext-dev \
  libgtk2.0-dev \
  libgtk-3-dev \
  libglib2.0-dev \
  libpango1.0-dev \
  libfontconfig1-dev \
  libfreetype6-dev \
  libstartup-notification0-dev \
  libasound2-dev \
  libcurl4-openssl-dev \
  libssl-dev \
  libdbus-glib-1-dev \
  lsb-release \
  libjack-dev \
  libiw-dev \
  mesa-common-dev \
  libnotify-dev \
  libxrender-dev \
  libpulse-dev \
  nasm \
  yasm \
  unzip \
  dbus-x11 \
  xvfb \
  python3 \
  libffi-dev \
  clang \
  libclang-dev \
  llvm-dev \
  rustc \
  nodejs-mozilla \
  git \
  wget \
  xz-utils \
  curl \
  rustc \
  cargo

export PATH=${PATH}:/root/.cargo/bin
cargo install --version 0.19.0 cbindgen

export MOZ_DISABLE_CLEAN_CHECKS=1
cd ${work_dir}
dpkg-source -x librewolf_${distro_firefox_version}.dsc ${output_dir}
cd ${output_dir}
dpkg-buildpackage -us -uc -nc -d
